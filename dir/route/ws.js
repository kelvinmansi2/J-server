const router = require('express').Router()//express router module

const ws = require('../controller/ws')//ws controller

const { auth0 } = require('../configuration/auth')///auth middleware for firebase token verification

router.post('/ws', auth0, ws.result)//route for ws instance or request

module.exports = router//exporting router instance