const { fire } = require('./admin-firebase/firebase-admin-init')//firebase-admin instance

//token auth module using firebase 
//gets token sent from client for verification using firebase admin instance 
auth0 = async function (req, res, next){
              
  const { authorization } = req.headers//getting user token from header 

//checkig if { authorization } is empty 
  if(authorization == null){
      return res.sendStatus(401)
  }
//send token to firebase to check its validity 
  try {
    await fire.auth().verifyIdToken(authorization).then(__=>{
      next(true)
    }).catch( error => res.sendStatus(401).json(error)).finally(__=>{return null})
  } catch (error) {
    res.sendStatus(401).json(error.message)
  }
              
}

module.exports = {
  auth0
}
