const admin = require('firebase-admin');
const serviceAccount = require('./serviceaccount.json');

module.exports = {
    admin,
    serviceAccount
}