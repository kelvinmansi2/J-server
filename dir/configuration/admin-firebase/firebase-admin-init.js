const { admin, serviceAccount } =  require('./firebase-admin')//firebase admin
//creating firebase instance for firebase-admin
const fire = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: process.env.FIREBASEURL
  });

module.exports = {
  fire
}