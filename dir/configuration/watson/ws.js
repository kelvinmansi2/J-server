const NaturalLanguageUnderstandingV1 = require('ibm-watson/natural-language-understanding/v1');//ntl module
const { IamAuthenticator } = require('ibm-watson/auth'); //IBM watson auth module 

//setting NLU instance 
const ntl = function (req, res, next){
  try {
    return new NaturalLanguageUnderstandingV1({
      version: '2019-07-12',
      authenticator: new IamAuthenticator({
        apikey: process.env.KEY,
      }),
      url: process.env.URL,
    });
  } catch (error) {
    res.json({
      msg: error
    })
  }

}
module.exports = {
  ntl
} 
