//watson params !!!!!important
//use to specify which watson respone objects {entities, sentiment, emotion .... } should be included
//'url': 'www.ibm.com',
const wsparams = {
    ///////////text url/////
    'features': {
      'entities': {
        'emotion': true,
        'sentiment': true,
        'limit': 2,
      },
      'keywords': {
        'emotion': true,
        'sentiment': true,
        'limit': 2,
      },
    },
  };


  module.exports =  { wsparams }