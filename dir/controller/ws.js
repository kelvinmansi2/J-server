const { ntl } = require('./../configuration/watson/ws')///Natural Language understanding instance
const { wsparams } = require('../configuration/watson/wsparams')///watson object parameters

///watson request module 
///passing feature obejcts to make api request to IBM watson
exports.result = async function(req, res, next) {
    
  try {
    await ntl.analyze(wsparams)
          .then(analysisResults => {
      res.json(analysisResults.result)
    })
    .catch(err => {
      res.json(err);
    });
  } catch (error) {
      res.json(error)
  }

}