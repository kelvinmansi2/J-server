const express = require('express')

const bodyParser = require('body-parser')

const cors = require('cors')

const morgan = require('morgan')

const ev = require('dotenv')

const wsrouts = require('./dir/route/ws')

ev.config()

const port = process.env.PORT

app = express()

// setup body parser middleware
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
 
// parse application/json
app.use(bodyParser.json())
 
app.use(cors())

app.use(morgan('dev'))

app.use('/api', wsrouts)

app.listen(port, ()=>{
    console.log(`started at port ${port}`)
})